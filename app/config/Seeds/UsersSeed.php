<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'loginid' => 'administrator',
                'password' => '$2y$10$.4Gh0RNHm8gkMVrD.qcw/uPuLJOMTN7NFN8Pp.a2PDlKQEDtF8WaS',
                'name' => 'システム管理者',
                'kana' => 'システムカンリシャ',
                'mail' => 'administrator@example.com',
                'suspend_flg' => '0',
                'note' => '開発用',
                'created' => '2020-06-26 07:15:17',
                'modified' => '2020-06-26 07:15:17',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}

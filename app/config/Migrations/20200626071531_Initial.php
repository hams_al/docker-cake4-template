<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public $autoId = false;

    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-up-method
     * @return void
     */
    public function up()
    {
        $this->execute("
CREATE TABLE IF NOT EXISTS `users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ユーザーID',
  `loginid` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL COMMENT 'ログインID',
  `password` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL COMMENT 'パスワード',
  `name` VARCHAR(32) NULL COMMENT '氏名',
  `kana` VARCHAR(32) NULL COMMENT 'フリガナ',
  `mail` VARCHAR(128) NULL COMMENT 'メールアドレス',
  `suspend_flg` TINYINT(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '利用停止フラグ',
  `note` TEXT NULL COMMENT '備考',
  `created` DATETIME NULL COMMENT '登録日時',
  `modified` DATETIME NULL COMMENT '更新日時',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `loginid_UNIQUE` (`loginid` ASC),
  UNIQUE INDEX `mail_UNIQUE` (`mail` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_general_ci
COMMENT = 'ユーザー';
");
        //$this->table('users')
        //    ->addColumn('id', 'integer', [
        //        'autoIncrement' => true,
        //        'comment' => 'ユーザーID',
        //        'default' => null,
        //        'limit' => null,
        //        'null' => false,
        //        'signed' => false,
        //    ])
        //    ->addPrimaryKey(['id'])
        //    ->addColumn('loginid', 'string', [
        //        'comment' => 'ログインID',
        //        'default' => null,
        //        'limit' => 32,
        //        'null' => true,
        //    ])
        //    ->addColumn('password', 'string', [
        //        'comment' => 'パスワード',
        //        'default' => null,
        //        'limit' => 255,
        //        'null' => true,
        //    ])
        //    ->addColumn('name', 'string', [
        //        'comment' => '氏名',
        //        'default' => null,
        //        'limit' => 32,
        //        'null' => true,
        //    ])
        //    ->addColumn('kana', 'string', [
        //        'comment' => 'フリガナ',
        //        'default' => null,
        //        'limit' => 32,
        //        'null' => true,
        //    ])
        //    ->addColumn('mail', 'string', [
        //        'comment' => 'メールアドレス',
        //        'default' => null,
        //        'limit' => 128,
        //        'null' => true,
        //    ])
        //    ->addColumn('suspend_flg', 'tinyinteger', [
        //        'comment' => '利用停止フラグ',
        //        'default' => '0',
        //        'limit' => null,
        //        'null' => false,
        //        'signed' => false,
        //    ])
        //    ->addColumn('note', 'text', [
        //        'comment' => '備考',
        //        'default' => null,
        //        'limit' => null,
        //        'null' => true,
        //    ])
        //    ->addColumn('created', 'datetime', [
        //        'comment' => '登録日時',
        //        'default' => null,
        //        'limit' => null,
        //        'null' => true,
        //    ])
        //    ->addColumn('modified', 'datetime', [
        //        'comment' => '更新日時',
        //        'default' => null,
        //        'limit' => null,
        //        'null' => true,
        //    ])
        //    ->addIndex(
        //        [
        //            'loginid',
        //        ],
        //        ['unique' => true]
        //    )
        //    ->addIndex(
        //        [
        //            'mail',
        //        ],
        //        ['unique' => true]
        //    )
        //    ->create();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->table('users')->drop()->save();
    }
}

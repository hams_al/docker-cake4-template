# Docker-CakePHP4 #


## ファイル構成 ##
```
doker-cake-template
　├ app
　│　├ config
　│　│　├ Migrations
　│　│　├ Seeds
　│　│　└ app.php
　│　├ src
　│　│　├ Controller
　│　│　├ Model
　│　│　└ View
　│　├ templates
　│　├ webroot
　│　│　└ index.php
　│　├ composer.json
　│　└ composer.lock
　├ docker
　│　├ mysql
　│　│　├ Dockerfile
　│　│　└ my.cnf
　│　├ options
　│　└ php-apache
　│　 　├ Dockerfile
　│　 　└ php.ini
　├ key
　├ .env
　└ docker-compose.yml
```
  
※.envファイルサンプル

```
APP_NAME=Docker-Cake4
DEBUG=true
SECURITY_SALT=18a4f2f367f703a86a8e6c571676388538eb96558567f63ae9bececd45550e46
TZ=Asia/Tokyo

MYSQL_HOST=local-db
MYSQL_DATABASE=app_db
MYSQL_USER=app_admin
MYSQL_PASSWORD=Local3gx1txbR0zWUTHwp7hTL5w6Y
MYSQL_ROOT_PASSWORD=LocalaycfCbleCMhumNQuaOaiU4ft

AWS_S3_ACCESS_KEY=LocalzcYC4MsuTdqhmG3LmjUkD4c7
AWS_S3_SECRET_KEY=Local7jvQJA322v48SYSK4wDirsDw
AWS_S3_DEFAULT_REGION=ap-northeast-1
AWS_S3_BUCKET=
```

---

## 開発環境構築手順 ##
1. .envファイルサンプルをコピーし、「.env」を作成する
2. Dockerイメージを作成する
3. コンテナを起動する
4. ComposerでVendorファイルをインストールする
5. データベースのマイグレーションをする

※以下のコマンド類を参考にしてください


---

## コマンド類 ##

### Dockerイメージの作成 ###

※phpとmysqlがビルドされる
```
$ docker-compose build
```


※Xdebugを有効にするとき
```
$ docker-compose -f docker-compose.yml -f docker/options/enable-xdebug.yml build
```

### 作成したイメージの確認 ###

※dockerコマンドであることに注意
```
$ docker images
```


### コンテナの起動 ###

※phpMyAdminのイメージがダウンロードされる。

```
$ docker-compose up -d
```

### 起動確認 ###

```
$ docker-compose ps
```

### ブラウザから確認 ###

※mac環境でSSLの証明書エラーが出る場合は、httpで確認。

```
https://localhost/info.php
http://localhost:8080/
```


### 停止 ###

```
$ docker-compose stop
```

---

### Composerを使ってVendorファイルのインストール ###

※composer.json、composer.lockを確認
```
$ docker-compose exec web composer install
```

※CakePHP初期画面表示
```
https://localhost/
```

---

### Migration ###
* マイグレーション
    ```
    $ docker-compose exec web bin/cake migrations migrate --no-lock
    ```
* 初期値投入
    ```
    $ docker-compose exec web bin/cake migrations seed
    ```
* ロールバック
    ```
    $ docker-compose exec web bin/cake migrations rollback --no-lock
    ```
* イニシャルファイル作成
    ```
    $ docker-compose exec web bin/cake bake migration_snapshot Initial --no-lock
    ```
* 初期データファイル作成
  ```
  $ docker-compose exec web bin/cake bake seed --data Users
  ```

---

### PHPコンテナに入って環境変数を確認 ###

```
$ docker-compose exec web /bin/sh
# printenv APP_NAME
```

### MySQLコンテナに入ってdump取得 ###

```
$ docker-compose exec local-db /bin/sh
# mysqldump -u app_admin -p --single-transaction --databases app_db > /tmp/app_db.sql
```

---

### キャッシュクリア ###

```
$ docker-compose exec web bin/cake cache clear_all
```

---

### 再ビルド手順（作成した環境を初期化） ###

1. composerキャッシュクリア

    ```
    $ docker-compose exec web composer clear-cache
    ```

2. コンテナ、イメージ、ボリューム、ネットワークの削除

    ```
    $ docker-compose down --rmi all --volumes
    ```
   
3. 残りのイメージ削除

    ※他案件で使ってるイメージも削除されるので注意。
    無理に消す必要はないかも。。
    
    ```
    $ docker rmi -f $(docker images -aq)
    ```
    
    以下のコマンドでIMAGE IDを特定して消す方法も。  
    
    ```
    $ docker images
    
    ex)
    IMAGE ID
    b230c73cd1db
    add3b2118f18
    642ac79dc240
    
    $ docker rmi b230c73cd1db 642ac79dc240 -f
    ```

4. /app/vendorフォルダの削除

5. ビルド

---

### オプション指定のビルド ###

※XDEBUGを有効にするとき

```
docker-compose -f docker-compose.yml -f docker/options/enable-xdebug.yml build
```